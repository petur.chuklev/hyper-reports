﻿using Hyper_Reports.Models.XmlModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Hyper_Reports.Repositories;
using Hyper_Reports.Entities;
using Hyper_Reports.Commands;
using System.Data.SqlClient;



namespace Hyper_Reports.Services
{
    public class XMLSerializer
    {
        public static void TakeData(string fileName)
        {
            Employee employee = new Employee();
            Entities.Department department = new Entities.Department();
            Entities.City city = new Entities.City();

            TurnoverRepository turnoverRepository = new TurnoverRepository();
            EmployeeRepository employeeRepository = new EmployeeRepository();
            CityRepository cityRepository = new CityRepository();
            DepartmentRepository departmentRepository = new DepartmentRepository();
            CompanyRepository companyRepository = new CompanyRepository();
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open))
                {
                    using (XmlReader reader = XmlReader.Create(fs))
                    {
                        XmlSerializer xmlSerializer = new XmlSerializer(typeof(Cities));
                        Cities cities = (Cities)xmlSerializer.Deserialize(reader);
                        foreach (var data in cities.CitiesData)
                        {
                            try
                            {
                                city = new Entities.City(data.Name);
                                cityRepository.Insert("City", city);
                            }
                            catch (SqlException) { }
                            foreach (var item in data.Departments.DepartmentsData)
                            {
                                try
                                {
                                    department = new Entities.Department(item.Name);
                                    departmentRepository.Insert("Department", department);
                                }
                                catch (SqlException) { }

                                try
                                {
                                    employee = new Employee(item.Employee,
                                        cityRepository.GetId(city),
                                        departmentRepository.GetId(department),
                                        companyRepository.GetId(new Company(SyncCommand.companyName)));
                                    employeeRepository.Insert("Employee", employee);
                                }
                                catch (SqlException) { }

                                try
                                {
                                    turnoverRepository.Insert("Turnover",
                                        new Turnover(SyncCommand.fileDate, item.Turnover, employeeRepository.GetId(employee)));
                                }
                                catch (SqlException)
                                {

                                    throw;
                                }
                            }
                        }
                    }
                }
            }
           
            catch (FileNotFoundException fnfe)
            {
                MessageMethods.DisplayMessage(fnfe.Message);
                MessageMethods.DisplayMessage(@"Check if the given directory is not empty or execute the ""config-data"" command again");

            }
            catch (DirectoryNotFoundException dnfe)
            {
                MessageMethods.DisplayMessage(dnfe.Message);
                MessageMethods.DisplayMessage(@"Try to execute the ""config-data"" command");
            }
            catch(IOException ioe)
            {
                MessageMethods.DisplayMessage(ioe.Message);
            }
        }
    }
}

