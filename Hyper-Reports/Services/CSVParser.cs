﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hyper_Reports.Models.CsvModels;
using TinyCsvParser;
using System.IO;
using Hyper_Reports.Entities;
using Hyper_Reports.Repositories;
using Hyper_Reports.Commands;
using System.Data.SqlClient;

namespace Hyper_Reports.Services
{
    public class CSVParser
    {
        private static CityRepository cityRepository = new CityRepository();
        private static CompanyRepository companyRepository = new CompanyRepository();
        private static EmployeeRepository employeeRepository = new EmployeeRepository();
        private static TurnoverRepository turnoverRepository = new TurnoverRepository();

        public static void ParseCSVData(string fileName)
        {
            CsvParserOptions csvParserOptions = new CsvParserOptions(true, ',');
            try
            {
                Data csvMapperForData = new Data();
                CsvParser<Data> csvParserForData = new CsvParser<Data>(csvParserOptions, csvMapperForData);
                var DataResult = csvParserForData
                    .ReadFromFile(fileName, Encoding.UTF8)
                    .ToList();
                foreach (var line in DataResult)
                {
                    City city = new City(line.Result.CityName);
                    try
                    {
                        cityRepository.Insert("City", city);
                    }
                    catch (SqlException) { }

                    Employee employee = new Employee(line.Result.EmployeeName,
                    cityRepository.GetId(city),
                    null,
                    companyRepository.GetId(new Company(SyncCommand.companyName)));

                    try
                    {
                        employeeRepository.Insert("Employee", employee);
                    }
                    catch (SqlException) { }

                    turnoverRepository.Insert("Turnover",
                    new Turnover(SyncCommand.fileDate,
                    line.Result.Turnover,
                    employeeRepository.GetId(employee))); 
                    
                }
            }
            catch (FileNotFoundException fnfe)
            {
                Console.WriteLine();
                Console.WriteLine(fnfe.Message);
                Console.WriteLine(@"Check if the given directory is not empty or execute the ""config-data"" command again");
                Console.WriteLine();
            }
            catch (DirectoryNotFoundException dnfe)
            {
                Console.WriteLine();
                Console.WriteLine(dnfe.Message);
                Console.WriteLine(@"Try to execute the ""config-data"" command");
                Console.WriteLine();
            }
            catch (IOException ioe)
            {
                Console.WriteLine();
                Console.WriteLine(ioe.Message);
                Console.WriteLine();
            }



        }
    }
}
