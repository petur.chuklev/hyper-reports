﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Hyper_Reports.Entities
{
   public class Company : NamedEntity
    { 
        public Company(string name)
        {
            Name = name;
        }

        public Company()
        {

        }
    }
}
