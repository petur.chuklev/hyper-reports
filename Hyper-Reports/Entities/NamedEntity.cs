﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hyper_Reports.Entities
{
    public class NamedEntity : BaseEntity
    {
        public string Name { get; set; }
        public NamedEntity()
        {

        }
    }
}
