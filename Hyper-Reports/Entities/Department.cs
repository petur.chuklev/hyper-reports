﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Hyper_Reports.Entities
{
    public class Department : NamedEntity
    {
        public Department(string name)
        {
            Name = name;
        }
        public Department()
        {

        }
    }
}
