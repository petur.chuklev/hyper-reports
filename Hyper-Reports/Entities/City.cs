﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Hyper_Reports.Entities
{
    public class City : NamedEntity
    { 
        public City(string name)
        {
            Name = name;
        }
        public City() { }
    }
}
