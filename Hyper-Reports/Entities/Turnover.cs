﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Hyper_Reports.Entities
{
    public class Turnover : BaseEntity
    {
        public DateTime Date { get; set; }
        public double Sum { get; set; }
        public int EmployeeID { get; set; }

        public Turnover(DateTime date, double sum, int employeeID)
        {
            Date = date;
            Sum = sum;
            EmployeeID = employeeID;
        }
        public Turnover()
        { }
    }
}
