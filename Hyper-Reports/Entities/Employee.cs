﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Hyper_Reports.Entities
{
    public class Employee : NamedEntity
    {
        public int CityID { get; set; }
        public int? DepartmentID { get; set; }
        public int CompanyID { get; set; }

        public Employee(string name, int cityID, int? departmentID, int companyID)
        {
            Name = name;
            CityID = cityID;
            DepartmentID = departmentID;
            CompanyID = companyID;
        }

        public Employee()
        {

        }
    }
}
