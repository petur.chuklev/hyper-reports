﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hyper_Reports.Entities;

namespace Hyper_Reports.Repositories
{
    public interface IQueryBuilder<T> where T : BaseEntity
    {
        List<T> Select();
        void Insert(string tableName, T entity);
        void Update(T entity);
        void Delete(string tableName, T entity);
    }
}
