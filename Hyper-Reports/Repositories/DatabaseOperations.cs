﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hyper_Reports.Entities;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Hyper_Reports.Services;
using System.Text.RegularExpressions;
using System.Xml;
using System.IO;

namespace Hyper_Reports.Repositories
{
    public class DatabaseOperations
    {
        private static string commandText;
        private static readonly string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        public static void GetReport(Dictionary<string, string> commands, string aggregation)
        {
            foreach (var input in commands)
            {
                if (input.Key == "period")
                {
                    Regex regex = new Regex(@"(year|month|quarter)-(\w{4})-?(\w{2})?");
                    MatchCollection matches = regex.Matches(input.Value);
                    switch (matches[0].Groups[1].Value)
                    {
                        case "year": commandText = GetCommand(aggregation, 
                            QueryForYear(matches[0].Groups[2].Value)); break;

                        case "month": commandText = GetCommand(aggregation, 
                            QueryForMonth(matches[0].Groups[3].Value, 
                            matches[0].Groups[2].Value)); break;

                        case "quarter": commandText = GetCommand(aggregation, QueryForQuarter(matches[0].Groups[2].Value,
                            Convert.ToInt32(new string(matches[0].Groups[3].Value.Last(), 1)))); break;

                        default: MessageMethods.DisplayMessage("Please choose valid period - year | month | quarter"); break;
                    }
                }
            }
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand DbCommand = connection.CreateCommand();
            DbCommand.CommandText = commandText;

            foreach (var command in commands)
            {
                if (command.Key == "period")
                {
                    continue;
                }
                IDataParameter sqlParameter = DbCommand.CreateParameter();
                sqlParameter.ParameterName = $"@{command.Key}";
                if ((command.Key == "topNumber") && (command.Value != null))
                {
                    sqlParameter.Value = $"{int.Parse(command.Value)}";
                }
                else if (command.Value == null)
                {
                    sqlParameter.Value = " ";
                }
                else
                {
                    sqlParameter.Value = command.Value;
                }
                DbCommand.Parameters.Add(sqlParameter);
                sqlParameter = null;
            }
            try
            {
                connection.Open();
                DbCommand.ExecuteNonQuery();
                IDataReader reader = DbCommand.ExecuteReader();
                using (reader)
                {
                    while (reader.Read())
                    {

                        string xmlResult = reader[0].ToString();
                        XmlDocument report = new XmlDocument();
                        report.LoadXml(xmlResult);
                        string dataPath = File.ReadAllText(@"../../ExportPath.txt");
                        report.Save(dataPath + $@"\{commands["companyName"]}-{commands["period"]}.xml");
                    }
                }
            }
            catch (SqlException sqle)
            {
                MessageMethods.DisplayMessage(sqle.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private static string GetCommand(string aggregation, string type)
        {
             
            return 
$"select TOP 20 SUM(t.Sum) as 'Turnover', {aggregation.First()}.Name from Company as co " +
"inner join Employee e on e.CompanyId = co.Id " +
"left join Turnover as t on t.EmployeeId = e.Id " +
"left join City as c on c.Id = e.CityId " +
"left join Department as d on d.Id = e.DepartmentId " +
$"where {type} and co.Name = @companyName " +
$"group by {aggregation.First()}.Name " +
"order by 'Turnover' " +
"for XML PATH ('TurnoverInfo'), ROOT('Report')";                                                                       
          
        }

        private static string QueryForYear(string year)
        {
            return $"Year(t.Date) = {year}";
        }

        private static string QueryForMonth(string month, string year)
        {
            return $"Year(t.Date) = {year} and Month(t.Date) = {month}";
        }

        private static string QueryForQuarter(string year, int quarter)
        {
            return $"Year(t.Date) = {year} and Month(t.Date) in ({quarter * 3}, {quarter * 3 - 1}, {quarter * 3 - 2})";
        }
    }
}
