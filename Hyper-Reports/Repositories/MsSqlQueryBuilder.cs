﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hyper_Reports.Entities;

namespace Hyper_Reports.Repositories
{
    public class MsSqlQueryBuilder<T> : BaseRepository<T> where T : BaseEntity, new()
    {
      
        public MsSqlQueryBuilder(string connectionString) : base(connectionString)
        {
        }
        
    }
}
