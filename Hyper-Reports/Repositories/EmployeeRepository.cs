﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hyper_Reports.Entities;

namespace Hyper_Reports.Repositories
{
    public class EmployeeRepository : NamedEntityRepository<Employee>, IQueryBuilder<Employee> 
    {
        public int GetIdByName(string name)
        {
            foreach (var employee in Select())
            {
                if (employee.Name == name)
                {
                    return employee.Id;
                }

            }
            return 0;
        }
        
    }
}
