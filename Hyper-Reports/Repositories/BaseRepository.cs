﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hyper_Reports.Entities;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Hyper_Reports.Repositories
{
    public class BaseRepository<T> : IQueryBuilder<T> where T : BaseEntity, new()
    {

        private readonly string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        public BaseRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public BaseRepository()
        {

        }

        private List<string> GetPropertiesNames(T entity)
        {
            List<string> names = new List<string>();
            foreach (PropertyInfo propInfo in entity.GetType().GetProperties())
            {
                if (propInfo.Name == "Id") continue;
                if ((propInfo.Name == "DepartmentID") && (propInfo.GetValue(entity) is null))
                {
                    continue;
                }
                names.Add(propInfo.Name);
            }
            return names;
       
        }

        private string GetParametersList(List<string> propertiesNames)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in propertiesNames)
            {
                sb.Append("@").Append(item).Append(", ");
            }
            return sb.ToString().Substring(0, sb.Length - 2); // For the ", "
        }

        private string GetColumnsList(List<string> propertiesNames)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in propertiesNames)
            {
                sb.Append(item).Append(", ");
            }

            return sb.ToString().Substring(0, sb.Length - 2);
        }

        private string GetColumnsAndPropertiesLines(List<string> propertiesNames)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in propertiesNames)
            {
                sb.Append(item).Append(" = ").Append("@").Append(item).Append(Environment.NewLine);
            }
            return sb.ToString().Substring(0, sb.Length - 1);
        } 

        public List<T> Select()
        {
            Type t = typeof(T);
            List<T> data = new List<T>();
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();
            command.CommandText =
                      $"SELECT * from {t.Name}";
            PropertyInfo[] props = t.GetProperties();
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                IDataReader reader = command.ExecuteReader();
                using (reader)
                {
                    while (reader.Read())
                    {
                        T entity = new T();
                        foreach (var prop in props)
                        {
                            var value = reader[$"{prop.Name}"];
                            if ((prop.Name == "DepartmentID") && (prop.GetValue(entity) is null))
                            {
                                continue;
                            }
                            prop.SetValue(entity, value);
                        }
                        data.Add(entity);
                    }
                }
            }
            
            finally
            {
                connection.Close();
            }
            return data;
        }

        public void Delete(string tableName, T entity)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();
            command.CommandText =
@"
use [HyperReports]
DELETE FROM @table
WHERE Id = @Id
";
            IDataParameter tableParameter = command.CreateParameter();
            tableParameter.ParameterName = "@table";
            tableParameter.Value = tableName;
            command.Parameters.Add(tableParameter);
            IDataParameter idParameter = command.CreateParameter();
            idParameter.ParameterName = "@Id";
            idParameter.Value = entity.Id;
            command.Parameters.Add(idParameter);
            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Insert(string tableName, T entity)
        {
            List<string> propertiesNames = GetPropertiesNames(entity);
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();
            command.CommandText =
                $@"
use [HyperReports]
INSERT INTO {tableName} 
({GetColumnsList(propertiesNames)})
VALUES 
({GetParametersList(propertiesNames)})";

            foreach (var pi in entity.GetType().GetProperties())
            {
                if (pi.Name == "Id") continue;
                if (pi.Name == "DepartmentID")
                {
                    if (pi.GetValue(entity) is null)
                    {
                        continue;
                    }
                }
                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = $"@{pi.Name}";
                parameter.Value = pi.GetValue(entity); // Така ще се получи ли? Кода преди: pi.Name
                command.Parameters.Add(parameter);
                parameter = null;
            }
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally 
            {
                connection.Close();
            }

        }

        public void Update(T entity)
        {
            List<string> propsNames = GetPropertiesNames(entity);
            string tableName = typeof(T).Name;
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();
            command.CommandText =
                $@"
use [HyperReports]
UPDATE {tableName} 
SET {GetColumnsAndPropertiesLines(propsNames)} 
WHERE Id=@Id 
";
          
            foreach (PropertyInfo pi in entity.GetType().GetProperties())
            {
                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = $"@{pi.Name}";
                parameter.Value = pi.GetValue(entity);
                command.Parameters.Add(parameter);
                parameter = null;
            }
            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
