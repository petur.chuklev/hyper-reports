﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Hyper_Reports.Repositories
{
    public class ConnectionClass
    {
      
        public static void CreateDatabase(string connectionString)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            connection.Open();
            IDbCommand createDBCommand = connection.CreateCommand();
            
            createDBCommand.CommandText = @"
USE [HyperReports]
IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = 'City')
Begin

Use [HyperReports]

CREATE TABLE dbo.City(
Id int identity(1,1) primary key not null,
Name varchar(50) unique not null
);

Use [HyperReports]

CREATE TABLE dbo.Company(
Id int identity(1,1) primary key not null,
Name varchar(50) unique not null
);

Use [HyperReports]

CREATE TABLE dbo.Department(
Id int identity(1,1) primary key not null,
Name varchar(50) unique not null
);

Use [HyperReports]

CREATE TABLE dbo.Employee(
Id int identity(1,1) primary key not null,
Name varchar(50) unique not null,
CityId int FOREIGN KEY REFERENCES City(Id),
DepartmentId int FOREIGN KEY REFERENCES Department(Id) null,
CompanyId int FOREIGN KEY REFERENCES Company(Id)

);

Use [HyperReports]

CREATE TABLE dbo.Turnover(
Id int Identity(1,1) NOT NULL Primary key,
Date DateTime NOT NULL,
Sum decimal(18,0) NOT NULL,
EmployeeId int FOREIGN KEY REFERENCES Employee(Id)
);
End;";

            createDBCommand.ExecuteNonQuery();
            connection.Close();
        }
    }
}
