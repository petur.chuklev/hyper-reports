﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hyper_Reports.Entities;

namespace Hyper_Reports.Repositories
{
    public class NamedEntityRepository<T> : BaseRepository<T>, IQueryBuilder<T> where T : NamedEntity, new()
    {
        public int GetId(T entity)
        {
            int id = 0;
            BaseRepository<T> baseRepository = new BaseRepository<T>();
            foreach (var entityInDb in baseRepository.Select())
            {
                if (entityInDb.Name == entity.Name)
                {
                    id = entityInDb.Id;
                    break;
                }
            }
            return id;
        }
    }
}
