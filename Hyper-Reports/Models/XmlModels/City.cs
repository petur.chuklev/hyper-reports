﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hyper_Reports.Models.XmlModels
{
    [Serializable]
    [XmlType("city")]
    public class City
    {
        [XmlAttribute("name")]
        public string Name{ get; set; }
        [XmlElement("departments")]
        public Departments Departments{ get; set; }
    }
}
