﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hyper_Reports.Models.XmlModels
{
    [Serializable]
    [XmlType("department")]
    public class Department
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlElement("employee")]
        public string Employee { get; set; }
        [XmlElement("turnover")]
        public double Turnover{ get; set; }

    }
}
