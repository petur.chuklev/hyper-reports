﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hyper_Reports.Models.XmlModels
{
    [Serializable]
    [XmlType("departments")]
    public class Departments
    {
        [XmlElement("department")]
        public List<Department> DepartmentsData { get; set; }
    }
}
