﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hyper_Reports.Models.XmlModels
{
    [Serializable]
    [XmlRoot("cities")]
    public class Cities
    {
        [XmlElement("city")]
        public List<City> CitiesData { get; set; }
    
    }
}
