﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser.Mapping;

namespace Hyper_Reports.Models.CsvModels
{
    public class Data : CsvMapping<Data>
    {
        
        public string CityName { get; set; }
        public string EmployeeName { get; set; }
        public double Turnover { get; set; }

        public Data() : base()
        {
            MapProperty(0, x => x.CityName);
            MapProperty(1, x => x.EmployeeName);
            MapProperty(2, x => x.Turnover);     
        }
    }
}
