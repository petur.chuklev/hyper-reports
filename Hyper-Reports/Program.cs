﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commander.NET;
using Hyper_Reports.Services;
using Hyper_Reports.Commands;
using System.IO;
using Hyper_Reports.Repositories;
using System.Configuration;


namespace Hyper_Reports
{
    class Program
    {
        static void Main(string[] args)
          {
            
            ConnectionClass.CreateDatabase(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
            while (true)
            {
                string[] input = Console.ReadLine().Split();
                
                try
                {
                    HyperReports conf = new CommanderParser<HyperReports>().Parse(input);
                }
                catch (System.Reflection.TargetInvocationException)
                {
                    Console.WriteLine();
                    Console.WriteLine(@"Invalid command! Try with: ""config-data --dataDir <path>"" or ""sync"" command");
                    Console.WriteLine();
                }
                catch(Commander.NET.Exceptions.CommandMissingException)
                {
                    Console.WriteLine();
                    Console.WriteLine(@"Invalid command! Try with: ""config-data/export --dataDir/--exportDir <path>"" or ""sync"" command");
                    Console.WriteLine();
                }
                catch(Exception ex) // другите са хванати в самите команди
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
            }
        }
    }
}
