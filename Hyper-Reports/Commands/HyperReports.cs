﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commander.NET.Attributes;
using System.IO;
using Hyper_Reports.Services;

namespace Hyper_Reports.Commands
{
    public class HyperReports
    {
        [Command("config-data")]
        public ConfigData configData;

        [Command("config-export")]
        public ConfigExport ConfigExport;

        [Command("sync")]
        public SyncCommand SyncCommand;

        [Command("report")]
        public ReportCommand ReportCommand;


        [CommandHandler]
        public void SyncCommandMethod(SyncCommand syncCommand)
        {

        }

        [CommandHandler]
        public void ConfigCommandMethod(ConfigData configDataCommand)
        {
            if (!Directory.Exists(ConfigData.dataDir))
            {
                MessageMethods.DisplayMessage("There is no source directory given or it does not exist");
                MessageMethods.DisplayMessage(@"Try to execute the ""config-data"" command giving existing directory");
            }
            else
            {
                try
                {
                    File.WriteAllText(@"../../DataPath.txt", ConfigData.dataDir);
                }
                catch (IOException ioe) // всички други exception-и са хванати
                {
                    MessageMethods.DisplayMessage(ioe.Message);
                }
                finally
                {
                    MessageMethods.DisplayMessage("The data path was conigured successfully!");
                }
            }
        }

        [CommandHandler]
        public void ReportCommandMethod(ReportCommand syncCommand)
        {

        }
    }
}
