﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using Hyper_Reports.Repositories;


namespace Hyper_Reports.Commands
{
    public class ReportCommand : ICommand
    {

        [Parameter("--company")]
        public string companyName;
        
        [Parameter("--period")]
        public string period;
        
        [Parameter("--aggregation")]
        public string aggregation;

        [Parameter("--top", Required = Required.No)]
        public string topNumber;

        [PositionalParameter(1, "--top", Required = Required.No)]
        public string sortType;

        public Dictionary<string, string> commands = new Dictionary<string, string>();


         void ICommand.Execute(object parent)
         {
            commands.Add("companyName", companyName);
            commands.Add("period", period);
            commands.Add("topNumber", topNumber);
            
            DatabaseOperations.GetReport(commands, aggregation);
         }
    }
}
