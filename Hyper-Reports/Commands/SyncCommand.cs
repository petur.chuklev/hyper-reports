﻿using Commander.NET.Interfaces;
using Hyper_Reports.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using Hyper_Reports.Entities;
using Hyper_Reports.Repositories;

namespace Hyper_Reports.Commands
{
    public class SyncCommand : ICommand
    {
        CompanyRepository companyRepository = new CompanyRepository();

        public static DateTime fileDate;
        public static string companyName;

        void ICommand.Execute(object parent)
        {
            try
            {
                string dataPath = File.ReadAllText(@"../../DataPath.txt");
                string exportPath = File.ReadAllText(@"../../ExportPath.txt");

                var files = Directory.EnumerateFiles(dataPath);
                foreach (var currentFile in files)
                {
                    Regex regex = new Regex(@"(\d{4}-\d{2}-\d{2})-(\w{3,14})+[.](\w{3})");
                    MatchCollection matches = regex.Matches(currentFile);

                    fileDate = DateTime.Parse(matches[0].Groups[1].Value);
                    companyName = matches[0].Groups[2].Value;

                    try
                    {
                        companyRepository.Insert("Company", new Company(companyName));
                    }
                    catch (Exception) { }
                   
                    switch (matches[0].Groups[3].Value)
                    {
                        case "csv": CSVParser.ParseCSVData(currentFile); break;
                        case "xml": XMLSerializer.TakeData(currentFile); break;
                    }
                    string fileName = currentFile.Substring(dataPath.Length + 1);
                    Directory.CreateDirectory($@"{dataPath}/ProcessedFiles");
                    Directory.Move(currentFile, Path.Combine($@"{dataPath}/ProcessedFiles", fileName));
                    }
                    MessageMethods.DisplayMessage("Files were read succesfully!");
                }
            catch (Exception e)
            {
                MessageMethods.DisplayMessage(e.Message);
            }
        }
    }
}
