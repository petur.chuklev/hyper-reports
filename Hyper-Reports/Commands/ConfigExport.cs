﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using System.IO;
using Hyper_Reports.Services;

namespace Hyper_Reports.Commands
{
    public class ConfigExport : ICommand
    {
        [Parameter("--exportDir")]
        public static string pathToExportDir;

        public void Execute(object parent)
        {
            try
            {
                Directory.CreateDirectory(pathToExportDir);
            }
            catch (UnauthorizedAccessException)
            {
                MessageMethods.DisplayMessage("You are not unauthorized to access this directory");
            }
            catch(PathTooLongException)
            {
                MessageMethods.DisplayMessage("The given path is too long, try to simplify it");
            }
            catch(IOException ioe)
            {
                MessageMethods.DisplayMessage(ioe.Message);
            }
            finally
            {
                File.WriteAllText(@"../../ExportPath.txt", pathToExportDir);
                MessageMethods.DisplayMessage("The export path was configured successfully!");
            }
        }
    }
}
