Hyper Reports transfers turnover daily reports data from different files into related database in order to provide more complex reporting functionality.

Hyper Reports is a CLI interface that supports both CSV and XML formats and provides monthly, quaterly and yearly reports. As well as Top N cities, departments and employees with highest/lowest turnover   

Milestones:  
   1. Parse data from CSV   
       _Process CSV data in the database for every customer_   
   2. Parse data from XML   
       _Process CSV data in the database for every customer_   
   3. Export data in SQL database   
	   _Prepare export in SQL databse for the reports_   
   4. Connecting with SQL without Entity Framework    
      _Using one command for every different entity going in the database via Generics and Reflection_    
   5. Report engine   
	   _Monthly, quarterly and yearly reports_    
   6. Top N report   
      _Top N for cities, departments and employees with highest/lowest turnover_   




create empty Db in SQL which name should be 'HyperReports'   

Commands:   


1.  config-data --dataDir (directory)  //Points the directory where the files are stored

2.  sync      //takes the files and adds them to the Db

3.  report --company (companyName) --period (period) --aggregation (aggregation) [--top (number)]   
//Gets a report for the Turnovers on specific requirements

the "period" must be one of the following:

*  month-(year)-(month)             / example: month-2019-08

*   year-(year)                     / example: year-2019

*   quarter-(year)-q(number)         / example: quarter-2019-q3

the "aggregation" must be one of the following:

*  city

*  department

*  employee

the report command I try with: report --company vitz --period month-2018-10 --aggregation city --top 5
